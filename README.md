# Report ROB


This is platform generates ROB REPORT reports for **GE2/1** and **ME0**. In addition, it saves the reports in a database for later queries.  

[https://monitoringgem-git-monitoringem.app.cern.ch](https://monitoringgem-git-monitoringem.app.cern.ch)


## Description of files in this repository:

   * `viewGem/` contains the modules of the package:

   * `manage.py` is the main module. This runs the project and allows you to have a local server.

   * `requirements.txt` Contains all the libraries to be installed for the operation of the project.

   * `.gitlab-ci.yml ` Is the pipelane test for gilab 
     

## Installation

**NOTE:** *This project was made in* ***Python 3.10.11***, this is the version that must be used:

1. Install ***Python 3.10.11***

2. Clone the repository
    ```

    git clone https://gitlab.cern.ch/udea/monitoringgem.git
    cd monitoringgem

    ```

3. Create the virtual environment, you can use the following commands:

    ```
    python3.10 -m venv name_venv
    ```
   **name_venv** is the name virtual inveroment, it is recommended to use **venv**

4. Activate virtual environment

    ### Ubuntu
   ```
   source name_venv/bin/activate
   ```

   ### MacOS
   ```
   source name_venv/bin/activate
   ```

   ### Windows
   ```
   .\name_venv\Scripts\activate
   ```
5. Install libraries
   ```
   pip install -r requirements.txt
   ```

## Launch local server (use tool)

To launch the server you must follow the steps below:

1. Launch local server 

```
python manage.py runserver
```
You must have active the virtual environment from the previous step

2. **When you launch the server it will give you a url, you can click it and it will open the tool (Image of what it looks like)**

![alt text](image-1.png)

3. **Using the tool**

![alt text](image.png)


## Contributing

This project is in progress and it requires some improvments. Therefore, if you have any suggestion that would make this better, please fork the repository and create a pull request. You can also simply open an issue with the tag "enhancement". Don't forget to give the project a star! Thanks!

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/improvments`)
3. Commit your Changes (`git commit -m 'Adding some improvment`)
4. Push to the Branch (`git push origin feature/improvments`)
5. Open a Pull Request

## Contact

* [A. Alexis Ruales B](https://www.linkedin.com/in/anderson-alexis-ruales-b27638199/?originalSubdomain=co) - arualesb.1@cern.ch
